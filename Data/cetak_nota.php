<?php
require('assets/lib/fpdf.php');
class PDF extends FPDF
{
function Header()
{   $this->SetLineWidth(0);
    $this->Line(10,7,200,7);
    $this->SetLineWidth(1);
    $this->Line(10,8,200,8);
    $this->SetLineWidth(0);
    $this->Line(10,9,200,9);

    $this->SetFont('Times','B',30);
    $this->SetTextColor(1, 162, 232);
    $this->Cell(30,10,'KOST-DESTYA');

    $this->Ln(10);
    $this->SetFont('Arial','i',10);
    $this->SetTextColor(0);
    $this->cell(30,10,'JL KETINTANG TIMUR PTT III NO 32, SURABAYA');

    $this->cell(102);
    $this->SetFont('Arial','bu',16);
    $this->cell(30,120,'Surabaya, '.base64_decode($_GET['uuid']).'');
     $this->Line(10,39,200,39);
    $this->SetLineWidth(1);
    $this->Line(10,40,200,40);
    $this->SetLineWidth(0);
    $this->Line(10,41,200,41);

    $this->Ln(5);
    $this->SetFont('Arial','i',10);
    $this->cell(30,10,'Telp/WA : 081-273-338-544');
     $this->Line(10,39,200,39);
    $this->SetLineWidth(1);
    $this->Line(10,40,200,40);
    $this->SetLineWidth(0);
    $this->Line(10,41,200,41);

    $this->Cell(90);
    $this->SetFont('Arial','',15);
    $this->Cell(30,10,'Kepada      : '.base64_decode($_GET['id-uid']).'');
     $this->Line(10,39,200,39);
    $this->SetLineWidth(1);
    $this->Line(10,40,200,40);
    $this->SetLineWidth(0);
    $this->Line(10,41,200,41);

    $this->Ln(5);
    $this->SetFont('Arial','i',10);
    $this->cell(30,10,'No Invoice : '.base64_decode($_GET['inf']).'');
     $this->Line(10,39,200,39);
    $this->SetLineWidth(1);
    $this->Line(10,40,200,40);
    $this->SetLineWidth(0);
    $this->Line(10,41,200,41);

    $this->cell(90);
    $this->SetFont('Arial','',13);
    $this->cell(30,10,'Pembayaran : '.base64_decode($_GET['bln']).'');
    $this->SetLineWidth(0);
    $this->Line(10,39,200,39);
    $this->SetLineWidth(1);
    $this->Line(10,40,200,40);
    $this->SetLineWidth(0);
    $this->Line(10,41,200,41);

}
function LoadData(){
	$link= mysqli_connect("localhost","root","");
	mysqli_select_db($link, "imk");
	$id=base64_decode($_GET['oid']);
	$data=mysqli_query($link, "select sub_transaksi.jumlah_beli,barang.nama_barang,barang.harga_jual,sub_transaksi.total_harga from sub_transaksi inner join barang on barang.id_barang=sub_transaksi.id_barang where sub_transaksi.id_transaksi='$id'");
	
	while ($r=  mysqli_fetch_array($data))
		        {
		            $hasil[]=$r;
		        }
		        return $hasil;
}
function BasicTable($header, $data)
{       $this->cell(55);
        $this->SetFont('Times','bu',20);
        $this->Cell(30,10,'BUKTI PEMBAYARAN');
        $this->Ln(10);
        $this->SetFont('Arial','B',12);
        $this->SetTextColor(0);
        $this->SetFillColor(172, 172, 172);
        $this->SetLineWidth(0.6); 
        $this->Cell(15, 7, "NO", 'LTR', 0, 'C', true);
        $this->Cell(90, 7, "DETAIL KAMAR KOST", 'LTR', 0, 'C', true);
        $this->Cell(43, 7, "BIAYA", 'LTR', 0, 'C', true);
        $this->Cell(43, 7, "TOTAL BIAYA", 'LTR', 0, 'C', true);
       
         
    $this->Ln();
    
    $this->SetFont('Arial','',12);
    foreach($data as $row)
    {   
        $this->Cell(15,7,$row['jumlah_beli'],1);
        $this->Cell(90,7,$row['nama_barang'],1);
        $this->Cell(43,7,"Rp ".number_format($row['harga_jual']),1);
        $this->Cell(43,7,"Rp ".number_format($row['total_harga']),1);
        $this->Ln();
    }

    $link= mysqli_connect("localhost","root","");
	mysqli_select_db($link, "imk");
	$id=base64_decode($_GET['oid']);

    $getsum=mysqli_query($link, "select sum(total_harga) as grand_total,sum(jumlah_beli) as jumlah_beli from sub_transaksi where id_transaksi='$id'");
	$getsum1=mysqli_fetch_array($getsum);
	$this->cell(15);
    $this->cell(90);
    $this->cell(43,7,'Sub total : ');
    $this->cell(43,7,'Rp '.number_format($getsum1['grand_total']).'',1);
    $this->Ln(10);
    session_start();
    $this->cell(146);
    $this->SetFont('Arial','',10);
    $this->cell(30,50,''.$_SESSION['username'].'');
    $this->Ln(10);
    $this->SetFont('Arial','',8);
    $this->cell(30,-10,'* Bukti Pembayaran ini Dicetak Otomatis di Aplikasi Kost-Destya');
    $this->Ln(5);
    $this->SetFont('Arial','',8);
    $this->cell(30,-10,'* Bukti Pembayaran ini Harap Disimpan');
    $this->cell(127);
    $this->SetFont('Arial','',9);
    $this->SetTextColor(0);
    $this->cell(30,13,'Diterima Oleh');
    // $this->Image('img/perso1.jpg',10,10,-300);

}
}
$pdf = new PDF();
$pdf->SetTitle('Invoice : '.base64_decode($_GET['inf']).'');
$pdf->AliasNbPages();
$header = array('NO', 'Detil Kamar Kost','Biaya' ,'Total Biaya');
$data = $pdf->LoadData();
$pdf->AddPage();
$pdf->Image('barcode.png',131,12,50,13);
$pdf->Image('barcode1.png',135 ,75,80,28);
$pdf->Image('barcode2.png',181 ,12,14,14);
$pdf->Ln(10);
$pdf->BasicTable($header,$data);
$filename=base64_decode($_GET['inf']);
$pdf->Output('','KOST-DESTYA/'.$filename.'.pdf');
?>